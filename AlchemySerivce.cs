using MightAndMagicAlchemy.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MightAndMagicAlchemy
{
    public class AlchemySerivce
    {
        private Potion[] MM7and8 = new[] {
            new Potion("Cure Wounds", PotionColor.Red, null),
            new Potion("Magic Potion", PotionColor.Blue, null),
            new Potion("Cure Weakness", PotionColor.Yellow, null),
            new Potion("Cure Poison", PotionColor.Purple, null, ("Cure Wounds", "Magic Potion")),
            new Potion("Cure Disease", PotionColor.Orange, null, ("Cure Wounds", "Cure Weakness")),
            new Potion("Awaken", PotionColor.Green, null, ("Cure Weakness", "Magic Potion")),
            new Potion("Preservation", PotionColor.Orange, PotionColor.Blue, ("Cure Disease", "Magic Potion")),
            new Potion("Stoneskin", PotionColor.Orange, PotionColor.Yellow, ("Cure Disease", "Cure Weakness")),
            new Potion("Haste", PotionColor.Orange, PotionColor.Red, ("Cure Disease", "Cure Wounds")),
            new Potion("Recharge Item", PotionColor.Green, PotionColor.Blue, ("Awaken", "Magic Potion")),
            new Potion("Harden Item", PotionColor.Green, PotionColor.Yellow, ("Awaken", "Cure Weakness")),
            new Potion("Bless", PotionColor.Green, PotionColor.Red, ("Awaken", "Cure Wounds")),
            new Potion("Cure Insanity", PotionColor.Green, PotionColor.Orange, ("Awaken", "Cure Disease")),
            new Potion("Shield", PotionColor.Purple, PotionColor.Blue, ("Cure Poison", "Magic Potion")),
            new Potion("Water Breathing", PotionColor.Purple, PotionColor.Yellow, ("Cure Poison", "Cure Weakness")),
            new Potion("Heroism", PotionColor.Purple, PotionColor.Red, ("Cure Poison", "Cure Wounds")),
            new Potion("Remove Fear", PotionColor.Purple, PotionColor.Orange, ("Cure Poison", "Cure Disease")),
            new Potion("Remove Curse", PotionColor.Purple, PotionColor.Green, ("Cure Poison", "Awaken")),
            new Potion("Divine Cure", PotionColor.White, null, ("Stoneskin", "Haste")),
            new Potion("Divine Restoration", PotionColor.White, null, ("Haste", "Recharge Item"), ("Harden Item", "Heroism")),
            new Potion("Divine Power", PotionColor.White, null, ("Recharge Item", "Harden Item")),
            new Potion("Cure Paralysis", PotionColor.White, null, ("Stoneskin", "Awaken"), ("Harden Item", "Cure Disease")),
            new Potion("Body Resistance", PotionColor.White, null, ("Stoneskin", "Recharge Item")),
            new Potion("Mind Resistance", PotionColor.White, null, ("Heroism", "Recharge Item")),
            new Potion("Fire Resistance", PotionColor.White, null, ("Haste", "Harden Item")),
            new Potion("Air Resistance", PotionColor.White, null, ("Haste", "Shield")),
            new Potion("Water Resistance", PotionColor.White, null, ("Harden Item", "Shield")),
            new Potion("Earth Resistance", PotionColor.White, null, ("Stoneskin", "Heroism")),
            new Potion("Might Boost", PotionColor.White, null, ("Heroism", "Cure Poison")),
            new Potion("Intellect Boost", PotionColor.White, null, ("Harden Item", "Awaken")),
            new Potion("Personality Boost", PotionColor.White, null, ("Recharge Item", "Awaken")),
            new Potion("Endurance Boost", PotionColor.White, null, ("Shield", "Cure Poison")),
            new Potion("Accuracy Boost", PotionColor.White, null, ("Stoneskin", "Cure Disease")),
            new Potion("Speed Boost", PotionColor.White, null, ("Haste", "Cure Disease")),
            new Potion("Luck Boost", PotionColor.White, null, ("Shield", "Heroism")),
            new Potion("Shocking Potion", PotionColor.White, null, ("Heroism", "Cure Disease"), ("Haste", "Cure Poison")),
            new Potion("Swift Potion", PotionColor.White, null, ("Recharge Item", "Cure Poison"), ("Shield", "Awaken")),
            new Potion("Noxious Potion", PotionColor.White, null, ("Harden Item", "Cure Poison"), ("Recharge Item", "Cure Disease")),
            new Potion("Freezing Potion", PotionColor.White, null, ("Shield", "Cure Disease"), ("Heroism", "Awaken")),
            new Potion("Flaming Potion", PotionColor.White, null, ("Stoneskin", "Cure Poison"), ("Haste", "Awaken")),
            new Potion("Stone To Flesh", PotionColor.Black, null, ("Cure Paralysis", "Heroism")),
            new Potion("Dragon Slaying", PotionColor.Black, null, ("Flaming Potion", "Shield")),
            new Potion("Rejuvenation", PotionColor.Black, null, ("Divine Restoration", "Preservation"), ("Divine Restoration", "Bless")),
            new Potion("Pure Might", PotionColor.Black, null, ("Might Boost", "Cure Disease")),
            new Potion("Pure Intellect", PotionColor.Black, null, ("Intellect Boost", "Cure Disease")),
            new Potion("Pure Personality", PotionColor.Black, null, ("Personality Boost", "Cure Poison")),
            new Potion("Pure Endurance", PotionColor.Black, null, ("Endurance Boost", "Awaken")),
            new Potion("Pure Accuracy", PotionColor.Black, null, ("Accuracy Boost", "Awaken")),
            new Potion("Pure Speed", PotionColor.Black, null, ("Speed Boost", "Cure Poison")),
            new Potion("Pure Luck", PotionColor.Black, null, ("Swift Potion", "Stoneskin"))
        };

        private Potion[] MM6 = new[] {
            new Potion("Cure Wounds", PotionColor.Red, null),
            new Potion("Magic Potion", PotionColor.Blue, null),
            new Potion("Energy", PotionColor.Yellow, null),
            new Potion("Protection", PotionColor.Orange, null, ("Cure Wounds", "Energy")),
            new Potion("Resistance", PotionColor.Green, null, ("Magic Potion", "Energy")),
            new Potion("Cure Poison", PotionColor.Purple, null, ("Cure Wounds", "Magic Potion")),
            new Potion("Bless", PotionColor.White, null, ("Cure Poison", "Magic Potion")),
            new Potion("Extreme Energy", PotionColor.White, null, ("Energy", "Protection")),
            new Potion("Haste", PotionColor.White, null, ("Energy", "Resistance")),
            new Potion("Heroism", PotionColor.White, null, ("Protection", "Cure Wounds")),
            new Potion("Restoration", PotionColor.White, null, ("Resistance", "Cure Poison")),
            new Potion("Stone Skin", PotionColor.White, null, ("Magic Potion", "Protection")),
            new Potion("Super Resistance", PotionColor.White, null, ("Magic Potion", "Resistance")),
            new Potion("Supreme Protection", PotionColor.White, null, ("Protection", "Resistance")),
            new Potion("Divine Cure", PotionColor.Black, null, ("Protection", "Restoration")),
            new Potion("Divine Magic", PotionColor.Black, null, ("Resistance", "Super Resistance")),
            new Potion("Divine Power", PotionColor.Black, null, ("Cure Poison", "Extreme Energy")),
            new Potion("Essence of Accuracy", PotionColor.Black, null, ("Energy", "Bless")),
            new Potion("Essence of Endurance", PotionColor.Black, null, ("Energy", "Supreme Protection")),
            new Potion("Essence of Intellect", PotionColor.Black, null, ("Magic Potion", "Stone Skin")),
            new Potion("Essence of Luck", PotionColor.Black, null, ("Cure Poison", "Supreme Protection")),
            new Potion("Essence of Might", PotionColor.Black, null, ("Cure Wounds", "Heroism")),
            new Potion("Essence of Personality", PotionColor.Black, null, ("Magic Potion", "Restoration")),
            new Potion("Essence of Speed", PotionColor.Black, null, ("Cure Wounds", "Haste")),
            new Potion("Rejuvenation", PotionColor.Black, null, ("Resistance", "Extreme Energy")),
        };

        public Potion[] GetForVersion(bool mm78)
        {
            var potions = mm78 ? MM7and8.ToList() : MM6.ToList();

            for (int i = 0; i < potions.Count; i++)
            {
                var p = potions[i];
                p.Combinations = potions.Where(x => x.SubstrateNames.Any(y => y.Contains(p.Name)))
                                    .Select(x => {
                                        var combination = x.SubstrateNames.First(y => y.Contains(p.Name));
                                        return (potions.First(y => y.Name == combination.First(z => z != p.Name)), x);
                                    }).ToArray();
                p.Substrates = p.SubstrateNames.Select(x => new[] { potions.First(y => y.Name == x.First()), potions.First(y => y.Name == x.Last()) }).ToArray();
            }

            return potions.ToArray();
        }
    }
}
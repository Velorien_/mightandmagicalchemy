using System.Linq;

namespace MightAndMagicAlchemy.Models
{
    public class Potion
    {
        public Potion(string name, PotionColor color, PotionColor? otherColor, params (string, string)[] substrates)
        {
            Name = name;
            Color = otherColor.HasValue ? new[] { color, otherColor.Value } : new[] { color };
            SubstrateNames = substrates.Select(x => new[] { x.Item1, x.Item2 }).ToArray();
        }

        public string[][] SubstrateNames { get; } 

        public string Name { get; }

        public Potion[][] Substrates { get; set; }

        public (Potion substrate, Potion product)[] Combinations { get; set; }

        public PotionColor[] Color { get; }

        public PotionType Type =>
            Color.Length == 2 ? PotionType.Layered :
            Color[0] == PotionColor.Black ? PotionType.Black :
            Color[0] == PotionColor.White ? PotionType.White :
            Color[0] < PotionColor.Green ? PotionType.Simple : PotionType.Compound;
    }

    public enum PotionType
    {
        Simple, Compound, Layered, White, Black
    }

    public enum PotionColor
    {
        Red, Blue, Yellow, Green, Purple, Orange, White, Black
    }
}